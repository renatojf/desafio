﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Builders.DotNetCore.Infrastructure
{
    public abstract class DomainEntity 
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }
}
