﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Builders.DotNetCore.WebApi.Controllers
{
   
    [Route("api/BinarySearchTree")]
    public class BinarySearchTreeController : Controller
    {
        // GET: api/BinarySearchTree       
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

      
        // POST: api/BinarySearchTree

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // PUT: api/BinarySearchTree/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
       
    }
}
