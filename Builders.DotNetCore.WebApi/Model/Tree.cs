﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Builders.DotNetCore.WebApi.Model
{
    public class Tree
    {


        public int id { get; set; }
        public int value { get; set; }
        public int left_id { get; set; }
        public int right_id { get; set; }
        public int parent_id { get; set; }  //(optional)
    }
}
