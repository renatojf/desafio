﻿using System;
using System.Threading.Tasks;

namespace Builders.DotNetCore.Data
{
    public interface UnitOfWork : IDisposable
    {
        Repository<TEntity> GetRepository<TEntity>() where TEntity : class;
        void FlushSession();
    }
}
