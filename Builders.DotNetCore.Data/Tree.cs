﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builders.DotNetCore.Data
{
    public class Tree
    {


        public int id { get; set; }
        public int value { get; set; }
        public int left_id { get; set; }
        public int right_id { get; set; }
        public int parent_id { get; set; }  //(optional)



    }
}
