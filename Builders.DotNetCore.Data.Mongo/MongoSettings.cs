﻿using Microsoft.Extensions.Configuration;

namespace Builders.DotNetCore.Data.Mongo
{
    public class MongoSettings
    {
        public string ConnectioString;
        public string Database;
        public IConfiguration configurationRoot;
    }
}
